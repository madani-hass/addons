#!/usr/bin/with-contenv bashio
# ==============================================================================
# Home Assistant Community Add-on: Repetier Server
# Configures Repetier
# ==============================================================================
readonly CONFIG="/etc/repetier/RepetierServer.xml"
declare ingress_entry

# Sets unique secret key used for signing cookies.
if ! bashio::fs.file_exists "/data/secret"; then
    echo "${SUPERVISOR_TOKEN}" > /data/secret
fi
secret=$(</data/secret)
sed -i "s/secret_key.*/secret_key = ${secret}/g" "${CONFIG}"

ingress_entry=$(bashio::addon.ingress_entry)
sed -i "s#%%ingress_entry%%#${ingress_entry}#g" "${CONFIG}"