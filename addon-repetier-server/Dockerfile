ARG BUILD_FROM=hassioaddons/debian-base:3.2.2
FROM ${BUILD_FROM}

# Set shell
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Setup base system
ARG BUILD_ARCH=amd64
RUN \
    REPETIER_VER="0.93.1" \
    \
    && ARCH="${BUILD_ARCH}" \
    && if [ "${BUILD_ARCH}" = "armv7" ]; then ARCH="armhf"; fi \
    \
    && curl -J -L -o /tmp/Repetier.deb \
      "http://download.repetier.com/files/server/debian-armhf/Repetier-Server-${REPETIER_VER}-Linux.deb" \
    \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        libfontconfig1=2.13.1-2 \
        nginx=1.14.2-2+deb10u3 \
    \
    && dpkg --unpack /tmp/Repetier.deb \
    && rm -rf \
        /tmp/* \
        /var{cache,log}/* \
        /var/lib/apt/lists/* \
        /etc/nginx \
    && mkdir -p /var/log/nginx \
    && touch /var/log/nginx/error.log

# Copy root filesystem
COPY rootfs /

# Build arguments
ARG BUILD_DATE
ARG BUILD_REF
ARG BUILD_VERSION

# Labels
LABEL \
    io.hass.name="repetier-server" \
    io.hass.description="Home Assistant Repetier Server add-on" \
    io.hass.arch="${BUILD_ARCH}" \
    io.hass.type="addon" \
    io.hass.version=${BUILD_VERSION} \
    maintainer="Madani IoT Team <iot@madani.co.id>" \
    org.opencontainers.image.title="Repetier Server" \
    org.opencontainers.image.description="Home Assistant Repetier Server add-on" \
    org.opencontainers.image.vendor="Madani Hass.io Addons" \
    org.opencontainers.image.authors="Madani IoT Team <iot@madani.co.id>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.url="https://gitlab.com/madani-hass/addons" \
    org.opencontainers.image.source="https://gitlab.com/madani-hass/addons" \
    org.opencontainers.image.documentation="https://gitlab.com/madani-hass/addons/README.md" \
    org.opencontainers.image.created=${BUILD_DATE} \
    org.opencontainers.image.revision=${BUILD_REF} \
    org.opencontainers.image.version=${BUILD_VERSION}
